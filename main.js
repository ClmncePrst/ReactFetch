async function getCharacters() {
//appel de l'API et récupértion de la réponse avec Fetch
    const response = await fetch("https://jsonplaceholder.typicode.com/users");
    const characterList = await response.json();
/* garde uniquement les informations en JSON 
 et affiche pour chaque personnage les informations demandées: nom, rue, code postal, ville */
    for (const oneCharacter of characterList) {
        console.log(oneCharacter.name);
        console.log(oneCharacter.address.street);
        console.log(oneCharacter.address.zipcode);
        console.log(oneCharacter.address.city);
        console.log("\n"); //séparation entre les différents perosnnages pour plus de lisibilité
    }
}
getCharacters(); //appel de la fonction créée